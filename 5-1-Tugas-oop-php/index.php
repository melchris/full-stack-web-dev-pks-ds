<?php

trait Hewan{
    public $nama,
            $darah=50,
            $jumlahKaki,
            $keahlian;

    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }

}

abstract class Fight {
    use Hewan;

    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan) {
        echo "{$this->nama} sedang diserang {$hewan->nama}";
        echo "<br>";
        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
    }

    protected function getInfo (){
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>"; 
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Attack Power: {$this->attackPower}";
        echo "<br>";
        echo "Defence Power: {$this->defencePower}";
    }

    abstract public function getInfoHewan();
}

class Elang extends Fight {
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan: Elang";
        echo "<br>";

        $this->getInfo();
    }
}

class Harimau extends Fight {
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    } 

    public function getInfoHewan(){
        echo "Jenis Hewan: Harimau";
        echo "<br>";
        $this->getInfo();
    }
}

class Spasi {
    public static function show(){
        echo "<br>";
        echo "===========================";
        echo "<br>";
    }
}

$harimau = new Harimau("Harimau Sumatra");
$harimau->getInfoHewan();

Spasi::show();

$elang = new Elang("Elang");
$elang->getInfoHewan();

Spasi::show();

$harimau->serang($elang);

Spasi::show();

$elang->getInfoHewan();

Spasi::show();

$elang->serang($harimau);


?>