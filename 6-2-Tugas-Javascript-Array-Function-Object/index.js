// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

// Jawaban Soal 1
daftarHewan.sort();
daftarHewan.forEach(function(item){
    console.log(item)
})

// Soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

// Function introduce(data)
function introduce(data){
  return 'Nama saya ' + this.data.name + ', umur saya ' + this.data.age + ' tahun' + 
    ', alamat saya di ' + this.data.address + ", dan saya punya hobby yaitu " + this.data.hobby ;
}

// Jawaban Soal 2
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3

// Function hitung_huruf_vokal()

function hitung_huruf_vokal(str1)
{
  var vowel_list = 'aeiouAEIOU';
  var vcount = 0;
  
  for(var x = 0; x < str1.length ; x++)
  {
    if (vowel_list.indexOf(str1[x]) !== -1)
    {
      vcount += 1;
    }
  
  }
  return vcount;
}

// Jawaban Soal 3
var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2);

// Soal 4
var numbers = [0, 1, 2, 3, 5];

function hitung(numbers) {
  if (numbers < 1) {
    return numbers += -2;
  } else if (numbers == 1) {
    return numbers -= 1;
  } else if (numbers == 2) {
    return numbers = 2;
  } else if (numbers == 3) {
    return numbers += 1;
  } else {
    return numbers += 3;
  }
}

// Jawaban Soal 4
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));