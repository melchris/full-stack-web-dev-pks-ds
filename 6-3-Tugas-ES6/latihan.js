const myFunction = (nama) => {
    console.log('Hi, ' + nama);
};

myFunction("Melisa"); // Hi, Melisa

const myFunction2 = (nama2) => {
    return "Hi, " + nama2;
};

console.log(myFunction2("Melisa")); // Hi, Melisa

const longSquare = () => {
    let a = 5;
    let b = 6;
    return "Area: " + (a * b) + "\n" +
    "Perimeter: " + (2*(a+b));
}

console.log(longSquare());

const fullName = 'John Doe';
 
const john = {fullName};

console.log(john);

const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()