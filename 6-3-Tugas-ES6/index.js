// Soal 1
// Calculate Area
const longSquare = () => {
    let a = 5;
    let b = 6;
    return "Area: " + (a * b) + "\n" +
    "Perimeter: " + (2*(a+b));
}
// Jawaban Soal 1
console.log("Jawaban Soal 1");
console.log(longSquare());

// Soal 2
const newFunction = (firstName, lastName) => {
    const nama = {
        firstName : firstName,
        lastName : lastName,
    }
    return firstName + " " + lastName;
}

// Jawaban Soal 2
console.log("=======================");
console.log("Jawaban Soal 2");
console.log(newFunction("William", "Imoh"));

// Soal 3
const newObject3 = (firstName, lastName, address, hobby) => {
    return firstName + " " + lastName + " " + address + " " + hobby;
}
// Jawaban Soal 3
console.log("=======================");
console.log("Jawaban Soal 3");
console.log(newObject3("Melisa", "Christanti", "Solo", "Mendaki Gunung"));

// Soal 4
let west = ["Will", "Chris", "Sam", "Holly"];
let east = ["Gill", "Brian", "Noel", "Maggie"];

let combinedArray = [...west, ...east];

console.log("=======================");
console.log("Jawaban Soal 4");
console.log(combinedArray);

// Soal 5
const planet = "earth";
const view = "glass";

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log("=======================");
console.log("Jawaban Soal 5");
console.log(before);

