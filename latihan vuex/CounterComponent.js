export const CounterComponent = {
    template: `
    <div>
        <p> State pada Component COunter {{ counter }} </p>
    </div>
    `,
    computed : {
        counter(){
            return this.$store.getters.counter
        }
    },
}