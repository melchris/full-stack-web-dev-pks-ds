<?php

namespace App\Listeners;

use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Mail;
use App\Events\CommentStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToRegister implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRegisterStoredEvent  $event
     * @return void
     */
    public function handle(NewRegisterStoredEvent $event)
    {
        Mail::to($event->user->email)->send(new RegisterMail($event->otp));
    }
}