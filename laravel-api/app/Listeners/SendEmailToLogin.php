<?php

namespace App\Listeners;

use App\Mail\LoginMail;
use Illuminate\Support\Facades\Mail;
use App\Events\CommentStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToLogin implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginStoredEvent  $event
     * @return void
     */
    public function handle(LoginStoredEvent $event)
    {
        Mail::to($event->user->email)->send(new LoginMail($event->otp));
    }
}