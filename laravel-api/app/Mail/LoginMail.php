<?php

namespace App\Mail;

use App\User;
use App\OtpCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LoginMail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $user;
    public $otp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->otp = $otp;
        $this->otp = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.comment.comment_login_mail')
                    ->subject('Full Stack Web Dev PKS DS')
                    ->to($user->email);
    }
}
