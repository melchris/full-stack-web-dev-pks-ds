<?php

namespace App\Http\Controllers;

use App\Comment;
// use App\Mail\PostAuthorMail;
// use App\Mail\CommentAuthorMail;
use App\Events\CommentStoredEvent;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * index
     *
     * @return void
     */

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store','update','delete']);
    }

    public function index()
    {
        //get data from table posts
        $comment = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $comment
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $comment
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'     => $request->post_id
        ]);

        // Memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comment));

        // // Ini dikirim kepada yang memiliki POST
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        // // Ini dikirim kepada yang memiliki COMMENT
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Comment $comment)
    {
        
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comment = Comment::findOrFail($comment->id);

        if($comment) {

            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login',
                ], 403);
            }

            //update post
            $comment->update([
                'content'     => $request->content,
                'post_id' => $request->post_id
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $comment = Comment::findOrfail($id);

        if($comment) {

            $user = auth()->user();
            
            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login',
                ], 403);
            }

            //delete post
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}
