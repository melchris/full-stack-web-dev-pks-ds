<?php

class Hewan {
    public $nama,
            $darah = 50,
            $jumlahKaki,
            $keahlian;

    public function __construct($nama, $darah, $jumlahKaki, $keahlian){
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }

    public function setNama($nama){
        $this->nama = $nama;
    }

    public function getNama(){
        return $this->nama;
    }

    public function setDarah($darah){
        $this->darah = $darah;
    }

    public function getDarah() {
        return $this->darah = ($this->darah - $this->attackPower / $this->defencePower);
    }

    public function getJumlahKaki(){
        return $this->getJumlahKaki;
    }

    public function setJumlahKaki($jumlahKaki){
        $this->jumlahKaki = $jumlahKaki;
    }

    public function setKeahlian($keahlian){
        $this->keahlian = $keahlian;
    }

    public function getKeahlian(){
        return $this->keahlian;
    }

    public function getInfoHewan(){
        $str = "{$this->nama} | {$this->darah} | {$this->jumlahKaki} | {$this->keahlian}";
        return $str;
    }
    public function atraksi() {
        $str = "{$this->nama} sedang {$this->keahlian}";
        return $str;
    }
}

class Fight {
    public $attackPower,
            $defencePower;

    public function __construct($attackPower, $defencePower){
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function setAttackPower(){
        $this->attackPower = $attackPower;
    }

    public function getAttackPower(){
        return $this->attackPower;
    }

    public function getDefencePower(){
        return $this->defencePower;
    }

    public function setDefencePower(){
        $this->defencePower = $defencePower;
    }

    public function serang(){
        $str = "{$this->nama} sedang menyerang {$this->nama}";
        return $str;
    }
    public function diserang(){
        $str = "{$this->nama} sedang diserang {$this->nama}, dan darah berkurang sebanyak {$this->getDarah}";
        return $str;
    }

    public function getFight(){
        $str = "{$this->attackPower} | {$this->defencePower}";
    }
}

// Harimau adalah inheritance dari Hewan
class Harimau extends Hewan {

    public function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackPower, $defencePower){
        parent::__construct($nama, $darah, $jumlahKaki, $keahlian);
        parent::__construct($attackPower, $defencePower);
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHarimau(){
        $str = parent::getInfoHewan() . parent::getFight();
        return $str;
    }
}

// Elang adalah inheritance dari Hewan
class Elang extends Hewan {

    public function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackPower, $defencePower){
        parent::__construct($nama, $darah, $jumlahKaki, $keahlian);
        parent::__construct($attackPower, $defencePower);
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }
    
    public function getInfoElang(){
        $str = parent::getInfoHewan() . parent::getFight();
        return $str;
    }
}

$harimau = new Harimau();
$elang = new Elang();

echo $harimau->setNama("Harimau");
echo $harimau->getNama();
echo "<br>";
echo $harimau->setKeahlian("Lari Cepat");
echo $harimau->getKeahlian();
echo "<br>";
echo $harimau->setAttackPower(7);
echo $harimau->getAttackPower();
echo "<br>";
echo $harimau->setDefencePower(8);
echo $harimau->getDefencePower();
echo "<br>";

echo "<hr>";
echo $elang->setNama("Elang");
echo $elang->getNama();
echo "<br>";
echo $elang->setKeahlian("Terbang Tinggi");
echo $elang->getKeahlian();
echo "<br>";
echo $elang->usetAttackPower(10);
echo $elang->getAttackPower();
echo "<br>";
echo $elang->setDefencePower(5);
echo $elang->getDefencePower();
echo "<br>";
echo $elang->getDarah();

echo "<hr>";
echo $harimau->atraksi();
echo $harimau->serang();
echo $harimau-diserang();
echo $harimau->getDarah();
echo "<br>";

echo $elang->atraksi();
echo $elang->serang();
echo $elang-diserang();
echo $elang->getDarah();
echo $elang->getDarah();
echo "<br>";

?>